<?php
function lg_social_links( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'list' => '',
		),
		$atts
	);

	// No 'list' specific in shortcode. Display all enabled social media
	if ( $atts['list'] == '' ) {
		ob_start();
		?>
		<div class="lg-social-media-cont">
			<ul class="lg-social-media">
				<?php
					$socialMedias = get_field( 'lg_social_media', 'option' );
				if ( is_array( $socialMedias ) || is_object( $socialMedias ) ) {
					foreach ( $socialMedias as $socialField ) {
						if ( $socialField && $socialField['enable'] == 1 ) {
							?>
									<li><a target="_blank" href="<?php echo $socialField['url']; ?>"><?php echo $socialField['icon']; ?></a></li>
								<?php
						}
					}
				}
				?>
			</ul>           
		</div>
		<?php
		return ob_get_clean();
	} else {
		$currentSocialMedias = explode( ',', $atts['list'] );
		$socialMedias        = get_field( 'lg_social_media', 'option' );

		ob_start();

		?>
		<div class="lg-social-media-cont">
			<ul class="lg-social-media">
		<?php
		if ( is_array( $currentSocialMedias ) || is_object( $currentSocialMedias ) ) {
			foreach ( $currentSocialMedias as $currentSocialMedia ) {
				if ( is_array( $socialMedias ) || is_object( $socialMedias ) ) {
					foreach ( $socialMedias as $key => $socialMedia ) {
						if ( stringToCompare( $currentSocialMedia ) == stringToCompare( $key ) && $socialMedia['enable'] == 1 ) {
							?>
								<li><a target="_blank" href="<?php echo $socialMedia['url']; ?>"><?php echo $socialMedia['icon']; ?></a></li>
							<?php
						}
					}
				}
			}
		}
		?>
			</ul>
		</div>
		<?php
		return ob_get_clean();
	}
}

add_shortcode( 'lg-social-links', 'lg_social_links' );

?>
