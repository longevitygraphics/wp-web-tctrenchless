<?php

//Custom Post Types
function create_post_type() {

    // Projects
    $projects_labels = array(
      'name'          => __( 'Projects' ),
      'singular_name' => __( 'Project' ),
    );

    register_post_type( 'projects',
        array(
          'labels'       => $projects_labels,
          'public'       => true,
          'has_archive'  => false,
		  'publicly_queryable'  => false,
          'menu_icon'    => 'dashicons-layout',
          'show_in_menu' => 'trans_canada_trenchless',
          'supports'     => array( 'thumbnail','title', 'editor', 'excerpt' ),
          'taxonomies'   => array('category', 'post_tag'),
        )
    );




    // Services
    $services_labels = array(
      'name'          => __( 'Services' ),
      'singular_name' => __( 'Service' ),
    );

    register_post_type( 'services',
        array(
          'labels'       => $services_labels,
          'public'       => true,
          'has_archive'  => false,
          'menu_icon'    => 'dashicons-location',
          'show_in_menu' => 'trans_canada_trenchless',
          'supports'     => array( 'thumbnail','title', 'editor', 'excerpt' ),
          'taxonomies'   => array('category', 'post_tag'),
        )
    );




    // Careers
    $careers_labels = array(
      'name'          => __( 'Careers' ),
      'singular_name' => __( 'Career' ),
    );

    register_post_type( 'careers',
        array(
          'labels'       => $careers_labels,
          'public'       => true,
          'has_archive'  => false,
          'menu_icon'    => 'dashicons-location',
          'show_in_menu' => 'trans_canada_trenchless',   #### Main menu slug
          'taxonomies'   => array('category', 'post_tag'),
        )
    );

}
add_action( 'init', 'create_post_type' );

?>
