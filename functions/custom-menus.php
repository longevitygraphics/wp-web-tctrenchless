<?php

function create_menu() {
    add_menu_page(
        __( 'Trans Canada Trenchless', 'Trans Canada Trenchless' ),
        'Trans Canada Trenchless',   
        'manage_options',
        'trans_canada_trenchless',
        '',
        'dashicons-admin-site',
        2
    );
}

add_action( 'admin_menu', 'create_menu' );

?>