<?php
/**
 * Template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="container">

			<article id="post-0" class="post error404 not-found text-center mb-lg">
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( '404', 'twentyeleven' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'Looks like the page you\'re looking for does not exist.', 'twentyeleven' ); ?></p>

					<a class="btn btn-primary" href="/">BACK TO HOME</a>
				</div><!-- .entry-content -->
			</article><!-- #post-0 -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
