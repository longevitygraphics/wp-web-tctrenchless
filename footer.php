<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<footer id="colophon" class="site-footer">
		
		<div id="site-footer" class="bg-primary">
			<div class="site-footer-logo">
				<a href="/"><?php echo wp_get_attachment_image( 211, 'full-size'); ?></a>
		 	</div>
			<div class="site-footer-copy small"> <?php echo get_field('footer_blurb', 'option'); ?> </div>
			<div class="site-footer-address"><?php get_template_part("/templates/template-parts/address-card"); ?></div>
			<div class="site-footer-social">
				<h2 class="text-uppercase h4">Keep in touch</h2>
				<?php echo do_shortcode('[lg-social-links]'); ?>
			</div>
			<div class="site-footer-nav"><?php get_template_part("/templates/template-parts/nav-footer"); ?></div>
		</div>
		
		<div id="site-legal" class="bg-gray-base clearfix">
			<div class="site-info"><?php get_template_part("/templates/template-parts/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/templates/template-parts/site-footer-longevity"); ?> </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<div class="scroll-to-top"><i class="fas fa-chevron-up"></i></div>

<?php wp_footer(); ?>

</body>
</html>
