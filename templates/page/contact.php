<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content page-contact">
			<main>
				<div class="container">
					<div class="body-copy clearfix">
						<div class="tct-address">
							<?php the_content(); ?>
							<ul class="list-unstyled address-icon">
								<li>
									<span class="fa-stack fa-lg">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
									</span>
									<span class="text-primary">
										<b> Head Office: </b><br>
									<?php
										echo do_shortcode('[lg-address1]') . '<br>' . do_shortcode('[lg-city]') . ', ' . do_shortcode('[lg-province]') . ' ' . do_shortcode('[lg-postcode]') ;
										echo get_field('address_2', 'options');
									 ?></span>
								</li>
								<li>
									<span class="fa-stack fa-lg">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-clock-o fa-stack-1x fa-inverse"></i>
									</span>
									<span class="text-primary">MON-FRI 8AM TO 5PM</span>
								</li>
								<li>
									<span class="fa-stack fa-lg">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
									</span>
									<strong><a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>">Lower Mainland Phone: <?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a>
										<br>
										<a href="tel:+1<?php echo do_shortcode('[lg-phone-alt]'); ?>">Interior Phone: <?php echo format_phone(do_shortcode('[lg-phone-alt]')); ?></a>
									</strong>
								</li>
							</ul>
						</div>
						<div class="contact-content">
							<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
						</div>
					</div>
				</div>

				<?php get_template_part( '/templates/template-parts/cta-flexible' ); ?>

			<!-- Current projects with map -->
			<section class="tct-map">
				<?php echo do_shortcode('[lg-map id=256]'); ?>
			</section>

			</main>
		</div>
	</div>

<?php get_footer(); ?>
