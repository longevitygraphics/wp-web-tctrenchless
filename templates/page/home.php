<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

<section class="home-slide">
	<div class="slide-copy-container">
		<?php get_template_part( '/templates/template-parts/slider-copy' ); ?>
	</div>
	<?php echo do_shortcode('[lg-slider id=104]'); ?>
</section>


<div id="primary">
	<div id="content" role="main" class="site-content">
		<main>

			<!-- Main body content including H1 -->
			<div class="container">
				<div class="body-copy">
					<?php the_content(); ?>
				</div>
			</div>

			<!-- CTA Bravo -->
			<section class="tct-cta-bravo mb-lg">
				<div class="container">
					<?php get_template_part( '/templates/template-parts/cta-bravo' ); ?>
				</div>
			</section>

			<!-- Services -->
			<div class="bg-alpha-light">
				<section>
					<div class="container mb-lg pt-lg pb-lg">
						<div class="text-center mb-lg">
							<h2>Trenchless Utility Construction Specialists</h2>
							<p>Trans Canada Trenchless offers trenchless solutions for all underground utility infrastructure from residential to municipal applications.</p>
						</div>
						<?php get_template_part( '/templates/template-parts/service-grid' ); ?>
					</div>
				</section>
			</div>

			<!-- Recent Projects -->
			<section class="mb-lg pt-lg">
				<div class="container">
					<div class="text-center">
						<h2>Our Recent Projects</h2>
						<p><a href="/projects/">Visit our gallery</a></p>
					</div>
					<?php get_template_part( '/templates/template-parts/project-grid' ); ?>
				</div>
			</section>

			<!-- Current projects with map -->
			<section class="tct-map mb-lg">
				<?php echo do_shortcode('[lg-map id=347]'); ?>
			</section>

			<?php get_template_part( '/templates/template-parts/cta-flexible' ); ?>

		</main>
	</div>
</div>

<?php get_footer(); ?>
