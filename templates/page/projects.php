<?php

/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

<div id="primary">
	<div id="content" role="main" class="site-content">
		<main>

			<?php
			if (shortcode_exists("instagram-feed")) {
				echo '<div class="container">' . do_shortcode("[instagram-feed]") . '</div>';
			} else {
				$project_lightbox = get_field('project_lightbox');
				if ($project_lightbox) :
			?>
					<div class="image-light-box">
						<?php foreach ($project_lightbox as $lightbox_image) : ?>
							<a href="<?php echo $lightbox_image['url']; ?>" data-lightbox="roadtrip"><img src="<?php echo $lightbox_image['url']; ?>" alt="<?php echo $lightbox_image['alt']; ?>"></a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			<?php } ?>
		</main>
	</div>
</div>

<?php get_footer(); ?>