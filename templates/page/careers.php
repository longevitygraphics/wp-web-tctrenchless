<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content page-careers">
			<main>
				
				<div class="container clearfix">
					<div class="body-copy">
						<div class="featureimage">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="careers-content">
							<?php the_content(); ?>
							<ul class="list-unstyled">
								<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-clock-o fa-stack-1x fa-inverse"></i>
								</span>
								<span class="text-primary">MON-FRI 8AM TO 5PM</span>
								</li>
								<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
								</span>
									<strong><a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></strong>
								</li>
							</ul>
							<a href="/contact/" class="btn btn-primary text-uppercase">Contact us today</a>
						</div>
					</div>
				</div>
				
				<?php get_template_part( '/templates/template-parts/careers-grid' ); ?>					

			</main>
		</div>
	</div>

<?php get_footer(); ?> 


