
				<?php if( have_rows('cta') ):
					while ( have_rows('cta') ) : the_row();
						
						switch ( get_row_layout()) {

							// Picture Card
							case 'picture_card':
								echo '<section class="tct-cta-alpha mb-lg">';
								echo '<div class="container">';
								get_template_part('/templates/template-parts/cta-card');
								echo "</div>";
								echo "</div>";
							break;
							
							// CTA-Charlie
							case 'cta-charlie':
								echo '<section class="tct-cta-alpha mb-lg pb-lg pt-lg">';
								echo '<div class="container">';
								get_template_part('/templates/template-parts/cta-charlie');
								echo "</div>";
								echo "</div>";
							break;

							// CTA-Delta
							case 'cta-delta':
								echo '<section class="tct-cta-alpha mb-lg pb-lg pt-lg">';
								echo '<div class="container">';
								get_template_part('/templates/template-parts/cta-delta');
								echo "</div>";
								echo "</div>";
							break;

							default:
								echo "<!-- nothing to see here -->";
							break;
						}

					endwhile; else : // no layouts found 
				endif; ?>