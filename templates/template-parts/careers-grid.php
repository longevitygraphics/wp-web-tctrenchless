<?php 	
	$projects_query = array(
        'showposts' => -1,
        'post_type' => 'careers',
    );

    $projects_query_results = new WP_Query( $projects_query );
?>

<div id="tct-careers" class="container">
	<div class="body-copy">
			
			<?php if ( $projects_query_results->have_posts() ) : $postCount = 0; ?>
			<h2 class="h3">Current Openings</h2>
				<ul  class="list-unstyled">
				<?php while ( $projects_query_results->have_posts() ) :  $projects_query_results->the_post(); ?>
					<li>
					
						<!-- Career Button -->
						<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#careers-<?php echo get_the_ID(); ?>" aria-expanded="false" aria-controls="careers-<?php echo get_the_ID(); ?>">
						  <?php echo get_the_title(); ?>
						</button>

						<!-- Career -->
						<div class="collapse <?php if($postCount == 0){echo "in";} ?>" id="careers-<?php echo get_the_ID(); ?>">
						  <div class="well">
						    <?php the_content(); ?>
						    <div class="text-center">
							    <a href="#" class="btn btn-primary text-uppercase">Apply Now</a>
						    </div>
						  </div>
						</div>

						<?php $postCount++; ?>

					</li>
				<?php endwhile; ?>
			<?php endif ?>
			</ul>

		</div>
	</div>
</div>
<?php wp_reset_query(); ?>

