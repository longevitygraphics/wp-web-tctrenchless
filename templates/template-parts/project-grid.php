<?php
	$projects_query = array(
        'showposts' => -1,
        'post_type' => 'projects',
    );

    $projects_query_results = new WP_Query( $projects_query );
?>

<div id="tct-projects" class="container">
	<div class="body-copy">
		<div class="flex-container">
			<?php if ( $projects_query_results->have_posts() ) : ?>
				<?php while ( $projects_query_results->have_posts() ) :  $projects_query_results->the_post(); ?>

						<a href="/projects/" class="thumbnail">
							<div class="img-cont">
								<?php echo get_the_post_thumbnail(); ?>
							</div>
							<!--<div class="caption">
								<h2><?php /*echo get_the_title(); */?></h2>
							</div>-->
						</a>

				<?php endwhile; ?>
			<?php endif ?>
		</div>
	</div>
</div>
<?php wp_reset_query(); ?>
