<section class="cta-bravo">
    <div class="flex-container">
		<?php if( have_rows('cta_bravo_item') ): ?>
			<?php while( have_rows('cta_bravo_item') ): the_row(); ?>
				    
				    <div class="thumbnail text-center">
					  
					  <?php echo wp_get_attachment_image( get_sub_field('bravo_icon'), 'full-size'); ?>

				      <div class="caption">
						<?php the_sub_field('bravo_copy'); ?>
						<a href="<?php the_sub_field('bravo_link'); ?>" class="btn btn-default">Learn more</a>
				      </div>
				    </div>

			<?php endwhile; ?>
		<?php endif; ?>
    </div>
</section>


