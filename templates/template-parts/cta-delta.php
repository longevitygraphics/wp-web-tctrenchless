
<section class="cta-delta">
	<div class="img-cont">
		<?php echo wp_get_attachment_image( get_sub_field('delta_image'), 'full-size'); ?>
	</div>
	<div class="cta-body"><?php the_sub_field('delta_copy'); ?></div>
</section>