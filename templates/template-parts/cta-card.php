
<section class="cta-card">
	<div class="img-cont">
		<?php echo wp_get_attachment_image( get_sub_field('cta_card_picture'), 'full-size'); ?>
	</div>
	<div class="cta-body"><?php the_sub_field('cta_card_copy'); ?></div>
</section>