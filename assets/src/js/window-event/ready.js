// Windows Ready Handler
// Windows Ready Handler
 	
(function($) {
	
    $(document).ready(function(){
        var offset = 100;
        var speed = 250;
        var duration = 500;
        $('.scroll-to-top').on('click', function(){
                $('html, body').animate({scrollTop:0}, speed);
                return false;
        });
    });

}(jQuery));



//=require ../components/nav.js n