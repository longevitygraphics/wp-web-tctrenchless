<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="hidden"><?php echo file_get_contents(get_stylesheet_directory_uri().'/assets/dist/images/svg-icons.svg'); ?></div>

<div id="page" class="site">

	<header id="masthead" class="site-header">

		<div class="site-branding">

				<?php
					if(shortcode_exists('lg-site-logo')){
						echo do_shortcode('[lg-site-logo]');
					}else{
						the_custom_logo();
					}
			 	?>

		</div><!-- .site-branding -->

    <div class="main-navigation">
      	<nav class="navbar navbar-default">
      	    <!-- Brand and toggle get grouped for better mobile display -->
      	    <div class="navbar-header">
      	    <div class="navbar-toggle-container">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
                <span class="sr-only">Toggle navigation</span>
      	        <span class="icon-bar"></span>
      	        <span class="icon-bar"></span>
      	        <span class="icon-bar"></span>
      	      </button>
            </div>

              <?php
                if(shortcode_exists('lg-site-logo')){
                echo do_shortcode('[lg-site-logo]');
                }else{
                the_custom_logo();
                }
              ?>
      	    </div>


            <!-- Main Menu  -->
            <?php

              $mainMenu = array(
              	 'menu'              => 'main',
              	// 'theme_location'    => 'top-nav',
              	'depth'             => 2,
              	'container'         => 'div',
              	'container_class'   => 'collapse navbar-collapse',
              	'container_id'      => 'main-navbar',
              	'menu_class'        => 'nav navbar-nav navbar-right',
              	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
              		// 'walker'         => new WP_Bootstrap_Navwalker_Custom()  // Custom used in Skin Method.
              	'walker'            => new WP_Bootstrap_Navwalker()
              );
              wp_nav_menu($mainMenu);

            ?>
      	</nav>
    </div>

    <div class="site-header-utility">
       <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>">
		   Lower Mainland:
			<i class="fa fa-phone"></i>
			<span><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></span>
	   </a>
		<a href="tel:+1<?php echo do_shortcode('[lg-phone-alt]'); ?>">
			Interior :
			<i class="fa fa-phone"></i>
			<span><?php echo format_phone(do_shortcode('[lg-phone-alt]')); ?></span>
		</a>
    </div>

  </header><!-- #masthead -->

  <?php if ( ! is_front_page() ) : ?>
    <section class="bg-alpha">
      <div class="container text-center mb-lg pt-lg pb-lg">
		<?php if ( get_field( "use_h1" ) ) : ?>
			<h1 class="h2 text-white"><?php the_title(); ?></h1>
			<?php else : ?>
			<span class="h2 text-white"><?php the_title(); ?></span>
		<?php endif; ?>
      </div>
    </section>
  <?php endif; ?>
